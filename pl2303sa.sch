EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR03
U 1 1 5BF27898
P 6000 2700
F 0 "#PWR03" H 6000 2450 50  0001 C CNN
F 1 "GND" H 6005 2527 50  0000 C CNN
F 2 "" H 6000 2700 50  0001 C CNN
F 3 "" H 6000 2700 50  0001 C CNN
	1    6000 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J3
U 1 1 5BF27918
P 7200 2900
F 0 "J3" H 6971 2891 50  0000 R CNN
F 1 "USB_B_Micro" H 6971 2800 50  0000 R CNN
F 2 "digikey-footprints:USB_Micro_B_Female_10118193-0001LF" H 7350 2850 50  0001 C CNN
F 3 "~" H 7350 2850 50  0001 C CNN
	1    7200 2900
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP_Small C2
U 1 1 5BF27ADA
P 6700 2400
F 0 "C2" H 6788 2446 50  0000 L CNN
F 1 "16V10u" H 6788 2355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-1608-08_AVX-J_Pad1.25x1.05mm_HandSolder" H 6700 2400 50  0001 C CNN
F 3 "~" H 6700 2400 50  0001 C CNN
	1    6700 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5BF27B32
P 6000 2600
F 0 "C1" H 6092 2646 50  0000 L CNN
F 1 "0.1u" H 6092 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 2600 50  0001 C CNN
F 3 "~" H 6000 2600 50  0001 C CNN
	1    6000 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BF27C43
P 6300 2400
F 0 "R1" V 6200 2400 50  0000 L CNN
F 1 "1.5K" V 6200 2200 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6230 2400 50  0001 C CNN
F 3 "~" H 6300 2400 50  0001 C CNN
	1    6300 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5BF27CC1
P 6350 3050
F 0 "R2" V 6250 2900 50  0000 C CNN
F 1 "27" V 6250 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6280 3050 50  0001 C CNN
F 3 "~" H 6350 3050 50  0001 C CNN
	1    6350 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5BF27DBC
P 6350 3250
F 0 "R3" V 6250 3100 50  0000 C CNN
F 1 "27" V 6250 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6280 3250 50  0001 C CNN
F 3 "~" H 6350 3250 50  0001 C CNN
	1    6350 3250
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 5BF283A4
P 3750 2850
F 0 "J1" H 3644 3135 50  0000 C CNN
F 1 "CON" H 3644 3044 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3750 2850 50  0001 C CNN
F 3 "~" H 3750 2850 50  0001 C CNN
	1    3750 2850
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BF28490
P 4150 3000
F 0 "#PWR01" H 4150 2750 50  0001 C CNN
F 1 "GND" H 4155 2827 50  0000 C CNN
F 2 "" H 4150 3000 50  0001 C CNN
F 3 "" H 4150 3000 50  0001 C CNN
	1    4150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2950 4150 2950
Wire Wire Line
	4150 2950 4150 3000
Wire Wire Line
	6900 2900 6600 2900
Wire Wire Line
	6600 2900 6600 3050
Wire Wire Line
	6600 3050 6500 3050
Wire Wire Line
	6900 3000 6700 3000
Wire Wire Line
	6700 3000 6700 3250
Wire Wire Line
	6700 3250 6500 3250
Wire Wire Line
	6200 3050 5700 3050
Wire Wire Line
	5700 2950 5900 2950
Wire Wire Line
	5900 2950 5900 3250
Wire Wire Line
	5900 3250 6200 3250
Connection ~ 6600 2900
Wire Wire Line
	5800 2400 5800 2750
Wire Wire Line
	5800 2750 5700 2750
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5BF28969
P 5350 2000
F 0 "J2" V 5290 2048 50  0000 L CNN
F 1 "3V_ENABLE" V 5199 2048 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5350 2000 50  0001 C CNN
F 3 "~" H 5350 2000 50  0001 C CNN
	1    5350 2000
	0    1    -1   0   
$EndComp
Wire Wire Line
	5250 2200 4700 2200
Wire Wire Line
	4700 2200 4700 2950
Wire Wire Line
	4700 2950 4800 2950
Wire Wire Line
	5350 2200 5800 2200
Wire Wire Line
	5800 2200 5800 2400
Connection ~ 5800 2400
Wire Wire Line
	6000 2400 6000 2500
Wire Wire Line
	6000 2400 5800 2400
$Comp
L power:GND #PWR05
U 1 1 5BF2973A
P 6700 2500
F 0 "#PWR05" H 6700 2250 50  0001 C CNN
F 1 "GND" H 6705 2327 50  0000 C CNN
F 2 "" H 6700 2500 50  0001 C CNN
F 3 "" H 6700 2500 50  0001 C CNN
	1    6700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2850 5900 2850
Wire Wire Line
	5900 2850 5900 2200
Wire Wire Line
	5900 2200 6700 2200
Wire Wire Line
	6700 2200 6700 2300
$Comp
L power:GND #PWR02
U 1 1 5BF29EDD
P 4900 2400
F 0 "#PWR02" H 4900 2150 50  0001 C CNN
F 1 "GND" H 4905 2227 50  0000 C CNN
F 2 "" H 4900 2400 50  0001 C CNN
F 3 "" H 4900 2400 50  0001 C CNN
	1    4900 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2400 4800 2400
Wire Wire Line
	4800 2400 4800 2750
Wire Wire Line
	3950 2750 4550 2750
Wire Wire Line
	4550 2750 4550 2850
Wire Wire Line
	4550 2850 4800 2850
Wire Wire Line
	4800 3050 4500 3050
Wire Wire Line
	4500 3050 4500 2850
Wire Wire Line
	4500 2850 3950 2850
$Comp
L power:GND #PWR06
U 1 1 5BF2C8B6
P 7050 3350
F 0 "#PWR06" H 7050 3100 50  0001 C CNN
F 1 "GND" H 7055 3177 50  0000 C CNN
F 2 "" H 7050 3350 50  0001 C CNN
F 3 "" H 7050 3350 50  0001 C CNN
	1    7050 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3300 7200 3300
Wire Wire Line
	7050 3300 7050 3350
Wire Wire Line
	7050 3300 7200 3300
Connection ~ 7200 3300
Wire Wire Line
	6150 2400 6000 2400
Connection ~ 6000 2400
Wire Wire Line
	6450 2400 6600 2400
Wire Wire Line
	6600 2400 6600 2900
NoConn ~ 6900 3100
Wire Wire Line
	6700 2200 6900 2200
Wire Wire Line
	6900 2200 6900 2700
Connection ~ 6700 2200
$Comp
L MyLib:PL2303SA U1
U 1 1 5CB23229
P 4800 2750
F 0 "U1" H 5250 2975 50  0000 C CNN
F 1 "PL2303SA" H 5250 2884 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5460 2340 50  0001 C CNN
F 3 "" H 5460 2340 50  0001 C CNN
	1    4800 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
